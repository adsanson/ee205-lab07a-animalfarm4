///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   30_03_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include "node.hpp"

namespace animalfarm{

class Singlelinkedlist{
   protected:
   Node* head = nullptr;

   public:
      const bool empty() const;
      void push_front(Node* newNode);
      Node* pop_front();
      Node* get_first() const;
      Node* get_next(const Node* currentNode) const;
      unsigned int size() const;

  };
}
