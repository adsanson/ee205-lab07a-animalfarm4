///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   30_03_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

using namespace std;

namespace animalfarm{

class Node{
   protected:
      Node* next = nullptr;

      friend class Singlelinkedlist;
   };
}
