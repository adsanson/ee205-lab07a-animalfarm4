///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   30_03_2021
///////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include<cstdlib>
#include"list.hpp"

namespace animalfarm{
   Node* head = new Node();

   const bool Singlelinkedlist::empty() const{
      return head == NULL;
   }

   void Singlelinkedlist::push_front( Node* newNode ){
      newNode->next = head;
      head = newNode;
   }

   Node* Singlelinkedlist::pop_front(){
      if(head==NULL){
         return nullptr;
      }
      else{
         Node* temp = head;
         head = head->next;
         return temp;
   }
   }

   Node* Singlelinkedlist::get_first() const{
      return head;
   }

   Node* Singlelinkedlist::get_next( const Node* currentNode) const{
      return currentNode->next;
   }

   unsigned int Singlelinkedlist::size() const{
      unsigned int i;
      Node* ind = head;
      for(i=0; ind != NULL; i++){
         ind = ind->next;
      }
      return i;
   }
}
