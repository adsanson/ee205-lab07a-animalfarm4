///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   27_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <random>
#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm{

static random_device rd;
static mt19937_64 rng(rd());
static bernoulli_distribution boolrng(0.5);

Animal::Animal(){
   cout << "." ;
}

Animal::~Animal(){
   cout << "x" ;
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    
         return string("Male"); break; 
      case FEMALE:  
         return string("Female"); break;
      case UNKNOWN: 
         return string("Unknown"); break;
   }
   return string("Really, Really, Unknown");
};
	
const Gender Animal::getRandomGender(){
   if(boolrng(rng))
      return MALE;
   else
      return FEMALE;
}

string Animal::colorName (enum Color color) {
	switch (color){
   case BLACK:
      return("Black"); break;
   case WHITE:
      return("White"); break;
   case RED:
      return("Red"); break;
   case SILVER:
      return("Silver"); break;
   case YELLOW:
      return("Yellow"); break;
   case BROWN:
      return("Brown"); break;
   }
   return string("Unknown");
};

const enum Color Animal::getRandomColor(){
      uniform_int_distribution<> colorrng(BLACK, BROWN);
         int c = colorrng(rng);

         switch(c){
         case 0: 
            return BLACK; break;
         case 1: 
            return WHITE; break;
         case 2: 
            return RED; break;
         case 3: 
            return SILVER; break;
         case 4: 
            return YELLOW; break;
         case 5: 
            return BROWN; break;
         }
      return BLACK;
      }

const bool Animal::getRandomBool(){
      return boolrng(rng);
   }
const float Animal::getRandomWeight(const float from, const float to){
      uniform_real_distribution<> floatrng(from, to);
      float f = floatrng(rng);
      return f;
   }
   
   const string Animal::getRandomName(){
      uniform_int_distribution<> lengthrng(4, 15);
      int length = lengthrng(rng);
      
      uniform_int_distribution<> uppercaserng('A', 'Z');
      uniform_int_distribution<> lowercaserng('a', 'z');
      
      char randomname[length];
      
      randomname[0] = uppercaserng(rng);
      for(int i = 1 ; i <= length ; i++){
        randomname[i] = lowercaserng(rng);
      }
      randomname[length] = 0; 
      return randomname;
   }

}

